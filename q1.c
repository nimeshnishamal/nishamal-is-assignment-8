#include<stdio.h>
#include <stdlib.h>
#include<string.h>

struct student{
    char firstname[30];
    char subject[30];
    float marks;
};

int main(){
int stdcount=0;
printf("Please Enter The Number of Students Record: ");
scanf("%d",&stdcount);
struct student data[stdcount];
printf("\n-----------------------------------------------Please Enter the Details------------------------------------------------- \n");
for(int x=0;x<stdcount;x++){
    struct student student1;
    printf("Please Enter STUDENT_%d FIRST Name :- ",x+1);
    scanf("%s",&student1.firstname);
    printf("Please Enter Subjects :-");
    scanf("%s",&student1.subject);
    printf("Please Enter Marks :-");
    scanf("%f",&student1.marks);
    data[x]=student1;
    printf("\n************************************************************************************************************************");
}

printf("\n------------------------------------------------Displaying Student Informations----------------------------------------- \n");
for(int x=0;x<stdcount;x++){
    printf("Student%d:\n",x+1);
    printf("FIRST_NAME:%s\n",data[x].firstname);
    printf("SUBJECT:%s\n",data[x].subject);
    printf("MARKS:%.2f\n",data[x].marks);
}

return 0;
}
